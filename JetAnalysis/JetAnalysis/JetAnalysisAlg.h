#ifndef JETANALYSIS_JETANALYSISALG_H
#define JETANALYSIS_JETANALYSISALG_H

#include "AthenaBaseComps/AthAlgorithm.h"
#include <xAODJet/JetContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <TTree.h>
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"

class JetAnalysisAlg : public AthAlgorithm{
        public:
                JetAnalysisAlg (const std::string &name, ISvcLocator* pSvcLocator);
                virtual ~JetAnalysisAlg();

                virtual StatusCode initialize() override;
                virtual StatusCode execute() override;
                virtual StatusCode finalize() override;
        private:
                Gaudi::Property<float> m_electronPtCut {this, "ElectronPtCut", 0.0, "Offline Electron Pt Cut"};
                TTree *m_tree;
                ServiceHandle<ITHistSvc> m_ntsvc;
                std::vector <float> *m_electron_eta;
                float m_eta;
};

#endif
