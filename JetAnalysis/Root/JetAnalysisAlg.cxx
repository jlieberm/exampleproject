#include <JetAnalysis/JetAnalysisAlg.h>

JetAnalysisAlg :: JetAnalysisAlg (const std::string &name, ISvcLocator* pSvcLocator) : AthAlgorithm (name, pSvcLocator), m_ntsvc("THistSvc/THistSvc", name){

}

JetAnalysisAlg :: ~JetAnalysisAlg(){

}

StatusCode JetAnalysisAlg :: initialize(){
    m_tree = new TTree("events","events");
    m_tree->Branch("offline_el_eta",&m_electron_eta);
    m_ntsvc->regTree("/rec/", m_tree);
    return StatusCode::SUCCESS;
}

StatusCode JetAnalysisAlg :: execute(){
    m_electron_eta->clear();
    const xAOD::JetContainer *jets = nullptr;
    const xAOD::ElectronContainer *electrons = nullptr;
    // ATH_CHECK(evtStore()->retrieve(jets,"AntiKt4EMPFlowJets"));
    // // for (item : lista)
    // for (auto *jet : *jets){
    //     ATH_MSG_INFO("Jet Momentum: " << jet->pt());
    // }
    ATH_CHECK(evtStore()->retrieve(electrons,"Electrons"));
    for (auto *electron : *electrons ){
        if (electron->pt() < m_electronPtCut) continue;
        float eta = electron->eta();
        m_electron_eta->push_back(eta);
        ATH_MSG_INFO("Electron Momentum: " << electron->pt());
    }
    m_tree->Fill();
    return StatusCode::SUCCESS;
}

StatusCode JetAnalysisAlg :: finalize(){
    ATH_MSG_INFO("Finalizing JetAnalysisAlg");
    
    return StatusCode::SUCCESS;
}
