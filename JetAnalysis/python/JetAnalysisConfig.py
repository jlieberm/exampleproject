from __future__ import print_function
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

def JetAnalysisCfg():
    result = ComponentAccumulator()
    JetAnalysisAlg = CompFactory.JetAnalysisAlg
    JetAnalysis = JetAnalysisAlg("JetAnalysisExecution")
    JetAnalysis.OutputLevel = 0

    result.addEventAlgo(JetAnalysis)
    return result

if __name__ == "__main__":
    from AthenaCommon.Configurable import Configurable
    Configurable.configurableRun3Behavior=1
    
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    from AthenaConfiguration.AllConfigFlags import ConfigFlags
    ConfigFlags.Exec.MaxEvents=10
    cfg = MainServicesCfg(ConfigFlags)
    cfg.merge(JetAnalysisCfg())
    cfg.run()
